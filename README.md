#### Task

- validate json schema files from **./task_folder/schema** folder
- validate json event files from **./task_folder/event** folder
- save script output to **validation_log.txt** file
- copy script output into this **README.md** file (see below)

#### Content

- zip archive with source files and folders: **task_backend_developer_november_2020.zip**
- task_description: **task_description.txt**
- validation Python script: **validation_script_ok.py**
- validation log (the script output): **validation_log.txt**
- model schema file from https://json-schema.org/draft/2019-09/schema

#### Notes

- added `test_file.schema` into **./task_folder/schema** folder to check script, 'cause all schemas seem to be valid
    
#### Validation log


`Validating schemas from D:\projects.Python\Welltory-test\task_folder\schema folder:

Validating Schema 1: cmarker_created.schema.
Schema validated OK

Validating Schema 2: label_selected.schema.
Schema validated OK

Validating Schema 3: sleep_created.schema.
Schema validated OK

Validating Schema 4: test_file.schema.
Error with the schema: 345 is not one of ['array', 'boolean', 'integer', 'null', 'number', 'object', 'string'].

Validating Schema 5: workout_created.schema.
Schema validated OK

---------------------------------------------
Total: 5 schemas.
Valid: 4 schemas.
Invalid: 1 schemas.

Validating event json files from D:\projects.Python\Welltory-test\task_folder\event folder:

Validating file 1: 1eba2aa1-2acf-460d-91e6-55a8c3e3b7a3.json:
File corresponds label_selected.schema.
Error with the file against label_selected.schema: 'id' is a required property.
Check event file structure and/or content.

Validating file 2: 297e4dc6-07d1-420d-a5ae-e4aff3aedc19.json:
File corresponds sleep_created.schema.
Error with the file against sleep_created.schema: 'source' is a required property.
Check event file structure and/or content.

Validating file 3: 29f0bfa7-bd51-4d45-93be-f6ead1ae0b96.json:
File is not a dictionary. Check event file requirements.

Validating file 4: 2e8ffd3c-dbda-42df-9901-b7a30869511a.json:
File does not correspond any given schema
Check event file requirements or place additional .schema file(s) into schema folder.

Validating file 5: 3ade063d-d1b9-453f-85b4-dda7bfda4711.json:
File does not correspond any given schema
Check event file requirements or place additional .schema file(s) into schema folder.

Validating file 6: 3b4088ef-7521-4114-ac56-57c68632d431.json:
File corresponds cmarker_created.schema.
Error with the file against cmarker_created.schema: 'cmarkers' is a required property.
Check event file structure and/or content.

Validating file 7: 6b1984e5-4092-4279-9dce-bdaa831c7932.json:
File does not correspond any given schema
Check event file requirements or place additional .schema file(s) into schema folder.

Validating file 8: a95d845c-8d9e-4e07-8948-275167643a40.json:
File does not have 'event' key. Check event file requirements.

Validating file 9: ba25151c-914f-4f47-909a-7a65a6339f34.json:
File does not correspond any given schema
Check event file requirements or place additional .schema file(s) into schema folder.

Validating file 10: bb998113-bc02-4cd1-9410-d9ae94f53eb0.json:
File corresponds sleep_created.schema.
Error with the file against sleep_created.schema: 'source' is a required property.
Check event file structure and/or content.

Validating file 11: c72d21cf-1152-4d8e-b649-e198149d5bbb.json:
File does not correspond any given schema
Check event file requirements or place additional .schema file(s) into schema folder.

Validating file 12: cc07e442-7986-4714-8fc2-ac2256690a90.json:
File corresponds label_selected.schema.
Error with the file against label_selected.schema: 'id' is a required property.
Check event file structure and/or content.

Validating file 13: e2d760c3-7e10-4464-ab22-7fda6b5e2562.json:
File corresponds cmarker_created.schema.
Error with the file against cmarker_created.schema: 'cmarkers' is a required property.
Check event file structure and/or content.

Validating file 14: f5656ff6-29e1-46b0-8d8a-ff77f9cc0953.json:
File corresponds sleep_created.schema.
Error with the file against sleep_created.schema: 'source' is a required property.
Check event file structure and/or content.

Validating file 15: fb1a0854-9535-404d-9bdd-9ec0abb6cd6c.json:
File corresponds cmarker_created.schema.
Error with the file against cmarker_created.schema: 'cmarkers' is a required property.
Check event file structure and/or content.

Validating file 16: ffe6b214-d543-40a8-8da3-deb0dc5bbd8c.json:
File corresponds cmarker_created.schema.
Error with the file against cmarker_created.schema: 'cmarkers' is a required property.
Check event file structure and/or content.

------------------------------------------
Total: 16 files.
Valid: 0 files.
Invalid: 16 files.`

