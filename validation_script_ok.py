# TODO сделать лог txt, чтоб туда всё записывалось

import json
import jsonschema
import sys  # comment this line you want console output (not file)
from pathlib import Path


if __name__ == "__main__":
    # comment this line and the very last line if you want console output (not file)
    sys.stdout = open("validation_log.txt", "w")

    # part 0: place schemas in dictionary
    # all schemas in dict; keys are schema filenames w\o extensions, values are files' content
    schemas_path = Path("task_folder/schema").absolute()
    schemas = {}

    for sch_path in schemas_path.iterdir():
        if not str(sch_path).endswith(".schema"):
            continue
        with open(sch_path, 'r') as sch_file:
            event = json.load(sch_file)
            schemas[sch_path.name.replace(".schema", "")] = event

    # part I: validating schemas
    # model schema downloaded from https://json-schema.org/draft/2019-09/schema
    model_schema = json.load(open("task_folder/model.schema", 'r'))
    print()
    print(f"Validating schemas from {schemas_path} folder:")

    count_valid = 0
    count_total = 0

    for event, schema in schemas.items():
        count_total += 1
        print()
        print(f"Validating Schema {count_total}: {event}.schema.")
        try:
            jsonschema.validate(schema, model_schema)
            count_valid += 1
            print(f"Schema validated OK")
        except Exception as ex:
            err_message = str(ex).partition('\n')[0]
            print(f"Error with the schema: {err_message}.")
    print()
    print("---------------------------------------------")
    print(f"Total: {count_total} schemas.")
    print(f"Valid: {count_valid} schemas.")
    print(f"Invalid: {count_total - count_valid} schemas.")
    print()

    # part II: validating event jsons
    events_path = Path("task_folder/event").absolute()
    print(f"Validating event json files from {events_path} folder:")

    count_valid = 0
    count_total = 0

    for ev_path in events_path.iterdir():
        print()
        count_total +=1
        print(f'Validating file {count_total}: {ev_path.name}:')
        if not str(ev_path).endswith(".json"):
            continue
        with open(ev_path, 'r') as event_file:
            data = json.load(event_file)
            if not isinstance(data, dict):
                print(f"File is not a dictionary. Check event file requirements.")
                continue
            if "event" not in data:
                print(f"File does not have 'event' key. Check event file requirements.")
                continue
            if data["event"] not in schemas:
                print(f"File does not correspond any given schema")
                print("Check event file requirements or place additional .schema file(s) into schema folder.")
                continue
            print(f"File corresponds {data['event']}.schema.")
            try:
                jsonschema.validate(data, schemas[data["event"]])
                print(f"File validated OK!")
                count_valid += 1
            except jsonschema.exceptions.ValidationError as ex:
                err_message = str(ex).partition('\n')[0]
                print(f"Error with the file against {data['event']}.schema: {err_message}.")
                print("Check event file structure and/or content.")
    print()
    print("------------------------------------------")
    print(f"Total: {count_total} files.")
    print(f"Valid: {count_valid} files.")
    print(f"Invalid: {count_total - count_valid} files.")

    # comment this line you want console output (not txt file)
    sys.stdout.close()
